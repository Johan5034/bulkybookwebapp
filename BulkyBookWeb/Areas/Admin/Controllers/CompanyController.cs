﻿using Microsoft.AspNetCore.Mvc;
using BulkyBook.DataAccess;
using BulkyBook.Models;
using BulkyBook.DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Mvc.Rendering;
using BulkyBook.Models.ViewModel;
using Microsoft.AspNetCore.Hosting;

namespace BulkyBookWeb.Controllers
{
    [Area("Admin")]
    public class CompanyController : Controller
    {
        readonly IUnitOfWork _unitOfWOrk;
       

        public CompanyController(IUnitOfWork unitOfWork)
        {
            _unitOfWOrk = unitOfWork;
           
        }
        public IActionResult Index()
        {
          //  IEnumerable<CoverType> coverTypes = _unitOfWOrk.CoverType.GetAll();
            return View();
        }

        //GET
        public IActionResult Upsert(int? id)
        {
            Company company = new();

            if (id == 0 || id == null)
            {
                // ViewBag.CategoryList=CategoryList;// Use of Viewbag
                // ViewData["CoverTypeList"] = CoverTypeList;
                //Create();
                return View(company);
            }
            else
            {
                company= _unitOfWOrk.Company.GetFirstOrDefault(u=>u.Id==id);
                return View(company);
                //Update();
            }       
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpSert(Company company)
        {
            if (ModelState.IsValid)
            {
                
                if (company.Id==0)
                {
                    _unitOfWOrk.Company.Add(company);
                    TempData["success"] = "Category created successfully";
                }
                else
                {
                    _unitOfWOrk.Company.Update(company);
                   TempData["success"] = "Category updated successfully";
                }
                _unitOfWOrk.Save();
                
                return RedirectToAction("Index");
            }
            return View(company);
        }
        //GET
      /*  public IActionResult Delete(int? id)
        {

            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var coverType = _unitOfWOrk.CoverType.GetFirstOrDefault(c => c.Id == id);
            if (coverType == null)
            {
                return NotFound();
            }

            return View(coverType);
        }

        //POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? id)
        {
            var coverType = _unitOfWOrk.CoverType.GetFirstOrDefault(c => c.Id == id);
            if (coverType == null)
            {
                return NotFound();
            }
            _unitOfWOrk.CoverType.Remove(coverType);
            _unitOfWOrk.Save();
            TempData["success"] = "Category deleted successfully";
            return RedirectToAction("Index");
        }
      */

        #region API CALLS
        [HttpGet]
        public IActionResult GetAll()
        {
            var companyList = _unitOfWOrk.Company.GetAll();
            return Json(new {data=companyList});
        }

        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            var company = _unitOfWOrk.Company.GetFirstOrDefault(c => c.Id == id);
            if (company == null)
            {
                return Json(new {success=false,message="Error while deleting"});
            }
        
            _unitOfWOrk.Company.Remove(company);
            _unitOfWOrk.Save();
            // TempData["success"] = "Category deleted successfully";
            return Json(new { success = true, message = "Deleted successfully" });
        }

        #endregion
    }
}
